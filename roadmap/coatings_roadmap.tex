\subsection{Coatings: roadmap to readiness}

\subsubsection{Background}

Because of the universality of coating thermal noise as a 
limit to performance of all the ground-based detectors, and 
the significant technical challenges posed, it is one
focus of the community. Thermal noise limits the mid-band 
design sensitivity of 
current detectors (aLIGO and adVirgo), enhanced versions of 
current detectors (A+ and AdV+), 2.5G cryogenic detectors 
(Voyager and KAGRA), and 3G detectors 
(Einstein Telescope-Low Frequency, or `ET-LF', and 
Cosmic Explorer or `CE'). 
In all these 
cases, the thermal noise is fundamentally connected to the 
mechanical and optical properties of the dielectric mirror 
coatings. While A+ and AdV+ will operate 
at room temperature with 1.06~$\mu \rm m$ lasers, the designs of the 
2.5G and 3G detectors are still evolving, so therefore are the 
requirements on the mirror coatings. For definiteness in the 
following discussion, we assume that Voyager will operate at 
123~K with a 2~$\mu \rm m$ laser, ET-LF will operate at 20~K with a 
2~$\mu \rm m$ laser, and that Explorer will operate at a temperature 
and wavelength that emerges as most advantageous given 
the experience with the enhanced 2G and 2.5G detectors.
 
The power spectral density of thermal noise is proportional to 
the operating temperature of the mirrors, the mechanical loss 
angle of the mirror coatings at that temperature, and the 
thickness of the mirror coating~\cite{Levin:1998}. For all cases, a 
reduction in the mechanical loss by approximately a factor of 
4 compared to the current technology (Ti:tantala/silica) will be 
adequate to meet thermal noise requirements. Another 
consideration is that the required number of layers in the 
mirror decreases as the index difference between the layers 
increases, so that high-index contrast materials will reduce the 
thermal noise even if the mechanical loss is unchanged. The 
situation in ET-LF is somewhat different, in that the thermal 
noise is dominated by the mirror suspension, which must be 
able to extract the thermal load imposed by the optical power 
absorbed in the mirror; in this case the optical absorption plays 
a key role in the thermal noise performance, and must be held 
in the range of 1~ppm.
 
In addition to meeting these requirements on mechanical loss, 
the mirrors also must meet optical specifications 
(absorption, scatter, figure error) even more stringent than 
those for current 
detector designs. These are near the state of the art but 
attainable for conventional ion-beam-sputtering (IBS) deposition 
of amorphous oxide mirrors; if other types of materials and 
deposition methods emerge as necessary to meet the 
mechanical loss requirements, significant tool development will 
be necessary.
 
Mechanical loss in amorphous materials is generally known to 
result from coupling of elastic energy into low energy excitations 
of the materials, generally thought of as two-level systems (TLS) 
in an effective double-well potential in some appropriate 
configuration coordinate. These TLS typically involve motions 
of several dozen atoms, and different TLS are responsible for 
losses at different temperatures (low barrier heights at low 
temperatures, higher barriers at higher temperatures). Reducing 
the mechanical loss thus requires reducing the density of TLS 
with the barrier heights pertinent to the operating temperature of 
the system. It is important to note that a process that reduces the 
losses for one temperature may increase it for another, if the 
distribution of TLS is reduced at one barrier height at the expense 
of increasing it at another.

\subsubsection{Current approaches to low-noise mirrors}

Approaches to reducing the mechanical loss fall into several 
categories: improved processing of conventional amorphous 
oxide or doped amorphous oxide materials, alternative 
amorphous materials -- typically semiconductors like amorphous 
silicon (a-Si) or silicon nitride (SiN), or crystalline semiconductor 
mirrors. \\

\noindent\textbf{Amorphous oxides} \\
Empirical results for amorphous oxide mirrors deposited by 
IBS show that the mechanical loss at room temperature is 
reduced by post-deposition annealing, and that the reduction 
is greater at higher annealing temperatures. The maximum 
annealing temperature is limited by crystallization of the film. 
One approach under investigation is to chemically frustrate 
crystallization through introduction of an appropriate dopant into 
the coating to increase the temperature at which crystallization 
occurs. Another empirical observation is that low deposition 
rates often correlate with reduced losses. A promising result 
combining these two approaches, low-rate deposition of 
Zr:tantala with an electron-cyclotron-resonance (ECR) sputtering 
system followed by high-temperature annealing produced a 
film with the 4-fold reduction in mechanical loss at room 
temperature necessary for A+ and AdV+ mirrors, though to 
this point the results vary from run to run so reproducibility is not 
yet established. It is also not yet clear whether this low-rate 
method is scalable to deposition of complete mirrors meeting 
optical specifications. It should also be noted that cryogenic loss 
measurements are not available, and changes in 
room-temperature losses and cryogenic losses often are 
anti-correlated, so implications of this material to cryogenic 
2.5G and 3G detectors is not yet clear.
 
A more speculative approach to suppressing crystallization and 
thus allowing higher annealing temperatures is to geometrically 
frustrate crystallization through the use of nano-layers, with each 
quarter-wave layer consisting of alternating layers of two 
materials with several nm thickness, thinner than the critical size 
for nucleation of crystallites. Suppression of crystallization has 
been observed in titania/silica nanolayers. Concerns such as 
homogeneity and scattering in the nanolayers has not yet been 
characterized.
 
Until recently, progress in reducing mechanical losses has been 
based on empirical studies alone. Recently, there has been 
progress in theoretical tools to guide the empirical work. Molecular 
dynamics methods enable calculation of the atomic structures that 
correspond to the energy spectrum of TLS in amorphous materials. 
Theoretical results correctly predict empirical data for the 
temperature dependence (at cryogenic temperatures) of losses in 
model systems like silica, and the trend in loss vs Ti-doping in 
tantala. Obtaining results accurate for room temperature losses 
requires computation of larger structural motifs corresponding to 
higher barrier TLS; extension to these regimes is currently under 
development. These methods also have suggested dopants whose 
promise has been borne out experimentally, e.g. Zr in tantala. 
Closely related are experimental methods to determine atomic 
structure in amorphous films via electron or X-ray diffraction 
techniques. These serve both as tests to inform the structural 
models, and can be correlated against experimental data to directly 
establish relations between structure and mechanical loss. A 
``virtuous circle'' of theoretical modeling, atomic structure 
characterization, and macroscopic property measurements 
(in particular mechanical loss) has reached the stage where the 
methods mutually reinforce and speed developments in the 
respective methods.
 
A theoretical concept that has emerged is that of an ultra-stable 
glass, i.e. one that has an atomic structure of low internal energy 
with greatly reduced density of TLS, which is inaccessible to 
conventional annealing processes on realistic time scales, but 
can be reached through vapor deposition due to the enhanced 
atomic mobility of the surface layer during deposition. According 
to this picture, deposition at elevated temperatures (to increase 
surface mobility) and at low rates (to give longer times for surface 
atoms to explore the energy landscape) should lower the density 
of TLS.
 
The first empirical example of such an ultrastable inorganic 
material, elevated temperature deposition of amorphous silicon 
with two-order-of-magnitude reduction in cryogenic mechanical 
losses compared to room-temperature deposition, indicates the 
physical reality of the concept. This result both shows that a-Si is 
a promising material for long-wavelength mirrors (though 
unacceptable levels of optical absorption, discussed below, 
remain an issue), and motivates the search for ultrastable 
amorphous oxides suitable for 1$\mu \rm m$ operation. The first systematic 
effort at elevated temperature deposition of an oxide, amorphous 
tantala, yielded lower losses at room temperature than obtained 
with conventionally deposited films, but the losses converged to 
similar values after annealing; results for cryogenic losses of these 
films are not yet available. Recent results for amorphous alumina 
show lower cryogenic losses even after annealing than in 
room-temperature deposited films, which appears, subject to 
further verification, the first example of an ultrastable amorphous 
oxide. It is also a promising material for a low-index layer in a 
cryogenic mirror, since its mechanical loss is almost an order of 
magnitude lower than silica at cryogenic temperatures. These 
studies are in an early stage; further work (theoretical and 
empirical) is necessary to establish whether the ultrastable glass 
concept is a route to a low-noise cryogenic mirror. \\

\noindent\textbf{Semiconductor materials} \\
The results for the mechanical loss of amorphous silicon deposited 
at elevated temperatures show that a mirror consisting of a-Si as a 
high-index layer together with silica as a low index layer would 
meet the thermal noise requirements for cryogenic detectors. The 
issue that remains in this case is the optical loss, which is more than 
an order of magnitude higher than typical requirements. This 
absorption is associated with `dangling bonds' in silicon atoms that 
are three-fold rather than four-fold coordinated. As such, this 
mechanism is not intrinsic, and depends strongly on deposition 
conditions and post-deposition processing. For example, low 
deposition rates, post-deposition annealing and annealing in 
hydrogen can all reduce the absorption significantly, and are topics 
of active current research. The absorption cross-section decreases 
with increasing wavelength, resulting in a strong preference for 
2~$\mu \rm m$ 
vs 1.5~$\mu \rm m$ wavelength operation, and probably eliminating this 
material for use at 1~$\mu \rm m$. It is also worth noting that the best results 
for cryogenic losses in a-Si have been obtained with films fabricated 
by evaporation rather than sputtering. Verification of these results in 
sputtered films is important, since meeting optical quality requirements 
is not practical via evaporative deposition methods.
 
Silicon nitride is another material widely used in the semiconductor 
industry with potential for low-noise mirrors. Low-pressure chemical 
vapor deposition (LPCVD) produces films with adequate mechanical 
loss that together with silica low-index layers could form a mirror 
meeting ET-LF or Voyager thermal noise specifications. IBS 
deposited silicon nitride has been also produced with a 1.8 reduction 
of mechanical losses at room temperature with respect the titania 
doped tantala.  The issue again is excess optical absorption, which 
currently would be more than order of magnitude too large to meet 
specifications, but, with further development, have the potential, 
unlike amorphous silicon, for use with 1$\mu \rm m$ lasers. Another possible 
concern is the high Young's modulus that makes the material more 
suited to silicon or sapphire rather than silica substrates. \\

\noindent\textbf{Multi-material coatings} \\
In this sense, the problem for semiconductor materials is the 
converse of that for oxide materials: the mechanical properties 
are adequate, but the optical absorption is too high. A general 
approach that can address this problem is the use of multi-material 
coatings, in which the top layers, where the optical intensity is 
highest, consist of materials with low optical absorption but too 
large mechanical loss, while the lower layers consist of materials 
with low mechanical loss but too large optical absorption. In this 
way, the limitations of the types of material can be traded off against 
each other. Examples exist of multi-material coatings based on 
demonstrated material properties that could meet the requirements 
of ET-LF.%; the method is reviewed in [Martin, 2018]. 
While the 
additional complexity of depositing such multi-material coatings 
could pose a fabrication challenge, especially if the optimal deposition 
method differs for the different materials involved, it remains an 
important option if simpler material combinations fail to meet 
requirements. \\
 
\noindent\textbf{Crystalline coatings} \\
Epitaxially grown coatings consisting of alternating layers of high 
and low index layers of crystalline materials present an alternative 
method that avoids the mechanical loss issues associated with TLS 
in amorphous materials. The best developed of these materials are 
AlGaAs/GaAs mirrors grown by molecular beam epitaxy (MBE) on 
GaAs substrates and then transferred by wafer-bonding methods to 
mirror substrates such as silica or silicon. The mechanical and optical 
losses measured in small cavities with mirrors made in this fashion 
appear adequate for 2.5G and 3G detectors.
 
The issues with respect to applying these as general solutions for 
gravitational wave interferometry are related to scaling. One scaling 
issue is associated with developing the infrastructure necessary for 
fabrication of \~40~cm (or larger) diameter optics: growth of the 
necessary GaAs crystal substrates, and development of MBE and 
wafer-bonding tools meeting the optical homogeneity requirements. 
An order of magnitude estimate of the cost of that effort is $\sim$\$40M. 
%[Cole \$\$]. 
The other issue is related to MBE growth, in which an areal 
density of discrete defects generally appears. While these defects can 
be avoided in experiments with small beams by alignment in small 
cavities, this would not be the case with beam sizes characteristic of 
full-scale detector configurations. The optical scatter, absorption, and 
elastic loss associated with these defects are not yet well characterized, 
nor is their anticipated density known. Samples with several inch 
diameters are currently being characterized to better understand these 
issues, which presumably should be clarified before the investment in 
scaling the tooling is seriously considered.

A less well-developed approach is the use of GaP/AlGaP crystalline 
coatings. These have the advantage of being able to be grown directly 
onto silicon, removing the need for transferring the coating between 
substrates. Initial measurements of the cryogenic mechanical loss of 
these coatings appear very promising for use in cryogenic gravitational 
wave detectors. However, significant work on refining deposition 
parameters and reducing optical absorption is likely to be required.

\subsubsection{Current research programs}
There are several current and planned programs devoted to 
developing low-thermal-noise mirror coatings suitable for enhanced 
2G, 2.5G, and 3G detectors. Participants are involved in all aspects 
of coating research, including various deposition methods, 
characterization of macroscopic properties at room and cryogenic 
temperatures, and atomic structure modeling and characterization. 
The recent incorporation into the project of several groups involved 
in coating deposition is particularly important, as the costs and time 
delays associated with commercial deposition of research coatings 
have been significant impediments to rapid progress. \\

\noindent\textbf{LIGO Scientific Collaboration} \\
There are approximately 10 U.S. university research groups 
participating in various aspects of coating research. In late 2017, 
a more coordinated effort and additional funding for these groups 
were initiated under the Center for Coatings Research (CCR), 
jointly funded by the Gordon and Betty Moore Foundation and the 
NSF. Work in the U.S. also importantly includes that in the LIGO 
Laboratory. These efforts are complemented by groups not formally 
affiliated with the CCR, notably that by the large optical coating group 
at U. Montreal. Other major LSC coatings research programs are those 
in GEO (U. Glasgow, U. Strathclyde, U. Hamburg, U. Hannover). The 
efforts of these groups are coordinated through biweekly telecons of 
the LSC Optics Working Group. \\
 
\noindent\textbf{Virgo} \\
There are again ~10 European universities involved in various 
aspects of the Virgo Coatings Research and Development 
Project, including new 
material research, metrology and more recently simulation. The 
proposal has been submitted to the EGO Council. Funding 
should come in 2019. The ViSIONs project, providing additional 
support for six of the French institutions, including the 
Laboratoire des Mat\'eriaux Avanc\'es (LMA), was approved in 
June 2018 and it is focused on the studying the relation between 
the physical properties of sputtered or evaporated materials and 
the structural and macroscopic properties of the deposited films. 
LMA is the only facility currently operational that is capable of 
producing coatings of the size and optical quality required for 
gravitational-wave interferometers. Nevertheless LMA has already 
started improving the uniformity of coating deposition in order to 
meet the challenges of the A+ and AdV+ detectors. The 
AdV+ project 
contemplates the possibility to use end cavity mirrors of 55~cm 
diameter. Therefore LMA has developed its own plan to upgrade 
their coaters and tools to deal with such increase of diameter and 
weight.

\subsubsection{Timelines and outlook}

\noindent\textbf{Timelines}\\
By far the most pressing timeline is for the enhanced 2G detectors. 
In the case of A+, allowing for a one-year pathfinder after the 
coating material and process are identified, the required research 
to identify a suitable mirror coating should be completed by May 
2020. This timeline implicitly assumes that the coating will be a 
sputter-deposited amorphous doped oxide, perhaps deposited at an 
elevated temperature, a slower than conventional rate and/or with a 
higher than conventional annealing temperature. It is unlikely that 
there is time in such a schedule to identify coatings and develop the 
required equipment for a process with more significant deviations 
from current deposition methods.
 
In recognition of the fact that meeting the enhanced 2G detector 
timeline requires doing basic research on a development schedule, 
the efforts of the LSC community are of necessity focused on these 
mirrors. That said, it is also recognized that the community shouldn't 
put itself again in such a situation, so a portion of the current research 
effort is devoted to establishing approaches to mirrors for 2.5G and 3G 
detectors. It is difficult to set research timelines for this effort, since the 
funding and construction schedules for these systems are not yet 
established. Another open question is the deposition process that will 
be required for the 2.5G and 3G mirrors; the further that process is from 
conventional IBS, the longer it is likely to take to develop suitable tooling. 
It seems that in any plausible scenario at least five years are available 
for research into the best approach to mirrors meeting 2.5G requirements. 
Results for these 2.5 G mirrors will, in turn, inform choices with respect to 
3G mirrors. It is therefore too soon to argue for a large investment in 
scaling deposition tools alternative to elevated-temperature IBS for 2.5G 
and 3G mirrors. That said, as the funding trajectories and interferometer 
architectures become better defined, it will be important to regularly 
re-evaluate the current understanding of potential mirror technologies, 
and make critical decisions, especially with respect to deposition tools 
with long development times and requiring major financial investments. \\

\noindent\textbf{Outlook}\\
The additional funding provided by the CCR for U.S. efforts in coating 
research, combined with the previously existing NSF support, leave 
these efforts with reasonably adequate funding in the near future. It 
would be helpful to add another deposition group to the effort, as 
there is currently more capacity for characterization of properties 
other than cryogenic mechanical losses than for synthesis. It is also 
important that the LIGO Laboratory continue with at least its current effort 
level, as their contribution to high-throughput mechanical loss 
characterization, optical scatter and homogeneity measurements, 
and their overall coordination of sample fabrication, distribution, 
and characterization is important to the LIGO Scientific Collaboration 
efforts.
 
In GEO, there is growing capacity for depositing coatings at 
Strathclyde, UWS and Hamburg. These coatings can be produced 
at a rate faster than it is possible to characterize their properties at 
cryogenic temperatures, and thus more resources devoted to such 
characterization are desirable. Currently, a novel type of ion-beam 
sputtering is being developed and tested at Strathclyde. While this 
is a promising research route, there are also plans to set up a large 
chamber with an industry ion source which should be capable of 
producing large and uniform coatings. This is an important priority 
in gaining access to more coating facilities which are suitable for the 
deposition of large, high-quality coatings. Initiating studies of 
GaP/AlGaP crystalline coatings, using hardware now installed in an 
MBE chamber at Gas Sensing Solutions Ltd, is also planned. This is 
an important parallel research direction to the development of 
amorphous coatings.

In Virgo, the activities are progressing at the pace compatible with 
the funding available from other projects. LMA and University of 
Sannio are able to provide high quality coatings at a rate higher than 
the existing characterization capability in Virgo coating research and 
development. The project 
ViSIONs takes care of only one specific aspect of the research, that 
is the impact and the understanding of deposition parameters on the 
coatings properties.

At present, there is only one group, LMA, capable of depositing coatings 
of a size and quality suitable for gravitational wave interferometers. While 
French CNRS through EGO is fully committed to continue supporting 
LMA as a research and coating facility for future gravitational wave 
detectors, a single 
source for any critical component is viewed as a potential risk to the 
whole community. 
There was a second system capable of such depositions, though 
subsequently defunded, at CSIRO in Australia. It seems prudent to 
re-establish this program, both to provide a second source for 
full-scale coatings, and as a significant contributor to the ongoing 
research efforts.
 
While there is a good level of coordination within the LSC coating 
research programs, and within the Virgo collaboration, the interaction 
between LSC and Virgo groups has been less effective. Current 
efforts underway to establish an agreement on how to manage these 
interactions to enable more efficient exchange of information will be 
invaluable in generating synergy between the programs, and avoiding 
unnecessary duplication of efforts.




