\subsection{Computing requirements}

Gravitational wave signals from compact binary coalescences 
(CBCs) are a primary science target for 3G detectors. 
Matched filtering is one of the main techniques employed for 
both CBC detection and parameter estimation.  This powerful 
method is the optimal linear filter for signals buried in Gaussian, 
stationary noise.  A problem with matched filtering is that it is 
extremely phase sensitive, and thus requires very accurate 
waveform models (templates).  At present, these templates are 
constructed by combining both analytical and numerical relativity.  
As we increase the parameter space to include more asymmetric 
mass systems, and higher order physical effects such as extra 
harmonics and eccentricity, the importance of numerical relativity 
will grow.  As a consequence, so too will the required investment 
in computing.

Currently, to cover the parameter search space, more than $10^5$ 
templates need to be generated.  As GW parameter estimation 
uses Bayesian inference, between $10^6$-$10^7$ templates need to be 
generated to extract accurate astrophysical information.  In both 
cases, each template is cross-correlated with the data and a 
Likelihood value is calculated.  Each likelihood involves the 
evaluation of a discretized, noise-weighted inner product. The 
array sizes involved in describing both the discretized data and 
templates can be very large, with the array length being a direct 
function of the low-frequency cutoff of the detector.  As we move 
towards better low frequency performance in 3G detectors, the 
signals will have a longer duration in the detector, and will 
therefore require larger array sizes to digitize the templates. 
The waveform generation and the likelihood calculation 
constitute the main computational bottleneck for matched filtering.

There are two possibilities for increasing the efficiency of GW 
astronomy, especially if the analysis becomes time-critical 
(e.g. an alert needs to be sent to EM and neutrino facilities). 
In the first case, we can rely on improvements in computer hardware. 
However, we are already seeing the time gap between jumps in 
technology beginning to lengthen, with a prediction that the well 
known Moore's law will come to an end ~2025.  In fact, as the cost 
of silicon chip fabrication increases, it may even be sooner than that. 
Furthermore, to offset the slowdown, a lot of current effort is being 
invested into multi-core chips using multithread technology. This 
suggests that to take advantage of future hardware, algorithms for 
GW astronomy might also need to be written in a multithreaded 
fashion, something that may be beyond the capabilities of the GW 
astronomers and require professional programmers.

The second option is to improve the convergence of our Bayesian 
inference algorithms.  A problem is that, in most cases, ``off the 
shelf'' algorithms do not work for GW astronomy without serious 
modification.  In most cases, it is possibly faster and simpler to 
develop specific GW algorithms.  However, algorithmic development 
is a slow process and is difficult to do while also trying to analyse 
real-time data.  As a consequence, this is something that may require 
dedicated effort.  

While it is difficult to predict the computational hardware and power 
that will be available in the 3G era, in the short to mid-term, the 
community should keep itself abreast of developments in both hardware 
and software.  We should keep in mind that not every new technology 
will be useful for GW astronomy, and some useful technologies may 
require too much effort for too small a return.  The community should 
be prepared to conduct cost-benefit exercises for those technologies 
that we believe to be pertinent.  And for those technologies that we 
decide to adopt, in order to fully maximize their impact, the community 
should be prepared to invest in employing expertise beyond the 
capabilities of GW astronomers, e.g. computer scientists, professional 
programmers. 







