\subsection{Progress on action items in the Dawn III report }

The Dawn III workshop \textit{What Comes Next for LIGO: Planning for the 
post-detection 
era in gravitational-wave detectors and astrophysics} took place on July 6-7, 
2017 in Syracuse, NY~\cite{DawnIII-talks}. This meeting 
focused on addressing strategic questions about the future of gravitational-wave 
astrophysics, and resulted in recommendations summarized in the Dawn III 
report~\cite{DawnIII-report}. \\

\noindent\textbf{Highlights of progress from Dawn II to Dawn III}

A series of achievements occurred prior to the Dawn III meeting that stemmed from 
recommended outcomes of the Dawn II meeting. The Gravitational Wave 
International Committee (GWIC) took the lead in organizing and fostering studies 
of 3G detectors, detector networks, and science. Additionally, the Gravitational Wave 
Agencies Correspondents (GWAC) played an increasingly impactful role in facilitating 
international funding agency communication. The Center for Coating Research was 
established to organize  international research aimed at the LIGO A+ upgrade and 
beyond. Progress was made toward achieving frequency-dependent squeezing in 
time for A+, and toward UK participation in LIGO A+. \\


\noindent\textbf{Major changes in the field between Dawn III and Dawn IV} 

The second observing run (O2) overall resulted in a number of detections, with 
discoveries of gravitational waves from the mergers of ten stellar remnant binary 
systems reported by the LIGO and Virgo collaboration to date~\cite{Abbott:2016blz, 
Abbott:2016nmj, TheLIGOScientific:2016pea, 
Abbott:2017vtc, Abbott:2017gyy, Abbott:2017oio, TheLIGOScientific:2018deq}. 
Additionally, the LIGO and Virgo 
collaboration achieved a global 2G detector network during O2, which made multiple 
detections, including the first triple detection of a binary black hole merger, 
GW170814~\cite{Abbott:2017oio}, and the observation 
of gravitational waves from a binary 
neutron star (BNS) merger, GW170817~\cite{TheLIGOScientific:2017qsa}. 
The latter discovery was the
 first example of multi-messenger astrophysics with gravitational waves. The
  presence of the Virgo detector in the LIGO-Virgo detector network enabled the sky 
  localization that drove a successful electromagnetic follow-up
   campaign~\cite{2041-8205-848-2-L12}. 
  GW170817 transformed the interaction between GW community and the broader 
  scientific community, establishing gravitational wave observations as a key 
  component of modern astronomy.  \\

\noindent\textbf{Progress toward future detectors }

The Dawn III report made six recommendations addressing global progress toward
 future detectors, including 2.5 G designs that are upgrades to current second 
 generation detectors, such as A+; and 3G designs such as the Einstein Telescope
  and Cosmic Explorer. Each of these recommendations is quoted below along 
  with a summary of progress. \\

\noindent \textit{``A+ should be implemented, and the team developing the upgrade concept should 
	submit a proposal as soon as possible."} 

The A+ project~\cite{Miller:2017plj} was recently funded by the 
		National Science Foundation in the U.S., extending the reach and project lifetime 
		of the LIGO detectors. A partner proposal is currently under review by the STFC 
		in the UK, providing significant in-kind contributions to LIGO A+. The Australian 
		OzGrav consortium is participating in A+, and has received funding 
		for A+ squeezing. The Virgo Collaboration has developed a two-phase 
		plan for AdV+, and is working with funding agencies on its realization. \\

\noindent \textit{``Essential A+ research and development must continue, in order to be ready to inform 
	the A+ final design.''} 

Several options for coatings are being 
		explored by the NSF-Moore funded Center for Coating Research (CCR), including 
		ideal glass, stabilized, and nanolayer coatings. A 300m scale filter cavity that would 
		enable frequency-dependent squeezing is now considered the baseline for the A+ 
		design. The LIGO laboratory and partners are currently exploring thermally-controlled 
		adaptive lenses designed to reduce mode mismatch loss. \\


\noindent \textit{``The timelines, ideal sensitivities, and realistic costs of the ultimate instrumentation of 
	existing 2G facilities (e.g., Voyager in the US) must be understood in order to make a 
	credible science case for new 3G facilities.''} 

This was a primary goal of the Dawn IV meeting; 
		see Section~\ref{sec:roadmap-common_strategies} for discussion. 
Additionally, seismic isolation and suspensions are under development for cryogenic silicon as 
		well as large room-temperature silica mirrors. Testing of 2 micron lasers and 
		development of light squeezing at 2 microns, needed for silicon mirrors, is underway.
Multiple groups around the world are also investigating alternative interferometer topologies. \\


\noindent \textit{``The lifetimes of the present 3- and 4-km installations should be soberly assessed to 
	help in determining timelines for 3G facilities.''} 
	
According to a LIGO Laboratory study the current LIGO facilities 
		are expected to last until the 
		mid-2040s with two major caveats: 1) the vacuum system is refurbished, which is underway for LIGO sites, and 2) funds are found to keep the remainder of the infrastructure from obsolescence. 
			$\sim$10M 
			USD are required beyond nominal vacuum system maintenance. \\

\noindent \textit{``An engineering study to establish scaling relations and to identify potential cost 
	reductions should begin as soon as proposed 3G concepts are sufficiently precise 
	to allow it.''} 
	
A collaborative proposal to study the science-driven requirements of a 3G network, 
		and perform a cost assessment for long above-ground detectors such as Cosmic 
		Explorer was funded by the NSF (MIT, Penn State, Syracuse, CSU Fullerton, Caltech). \\


\noindent  \textit{``Communication must be maintained among planners of 3G instruments (e.g., ET and 
	CE) to ensure that the gravitational wave community has a common science case, a 
	synergistic plan for the observatories, and a coherent message. The 3G science case 
	is the first priority.''}
	
The GWIC 3G subcommittees, especially the Science Case Team, have made 
		significant progress addressing this recommendation. 
		This Dawn IV meeting intended to further 
		deliver on this recommendation. \\
	

\noindent\textbf{The 3G science case} 

The GWIC 3G sub-committee has been charged to deliver the first draft of a 3G
science case document 
by December 2018, which will undergo internal review and revision in early 2019. 
The GWIC 3G Science Case Team has since been formed, and 
issued an open call to the international community to help develop the science case. 
This call attracted more than 200 researchers worldwide and still growing. The 
science case is being studied by nine working groups, each co-chaired by two or 
three members of the science case team.
The Dawn III report highlighted five priorities for building the global 3G science case. 
These are quoted below, along with a summary of progress. \\

\noindent \textit{``Access to a global network capable of resolving the polarization states of
	gravitational wave signals in addition to the position and distance to sources is 
	of critical importance for tests of General Relativity.''}

The observation of multiple BBH signals and one BNS merger demonstrated 
		the capability and value of a global 3-detector network.
GW170814 demonstrated that the signal's polarization is consistent with GR. More 
		than three detectors are required to test non-GR polarizations. KAGRA and 
		LIGO-India are keenly awaited. Additionally, GW170817 helped constrain the 
		propagation speed, and this helped rule out many alternative theories of gravity. \\


\noindent \textit{``The much improved sensitivity of 3G detectors will necessitate the development 
	of more accurate models to decode the ringdown phase of black holes and 
	establish whether the sources are Kerr black holes or something more exotic.''}
	
Theoretical efforts are underway to understand the dynamics of the merged 
		horizon and to generalize the no hair theorem to BBH systems. \\

\noindent \textit{``Concomitant with detector improvements, the relativity community
	should continue to deliver waveforms that cover a greater parameter space
	than is available today, in particular covering highly spinning, less massive systems,
	with much longer waveforms, and eccentric systems.''}

Numerical and analytical relativity efforts have been split between BBH and BNS (matter), but NR and AR groups are 
		coordinating to increase the parameter space for BBH simulations. \\


\noindent \textit{``To access the nuclear equation of state (EOS) under super-nuclear densities 
	attainable in neutron stars and understand how a binary neutron star (BNS) merger 
	might begin to inform the EOS, techniques need to be developed and tested that can
	derive neutron star radii from the data. This requires further development of
	codes capable of producing GR waveforms when taking into account matter effects.''}

We are working on improved waveform models, now that we have analyzed 
		GW170817 and reported the first measurement of NS radii. We need to understand 
		the model systematics and include the postmerger part of the signal in our analysis and 
		here too some progress has been made within the LVC. \\

\noindent \textit{``Detector performance is a multi-dimensional consideration. The community should 
	identify a set of performance metrics for future planning, beyond the single space-
	time volume metric (V x T) that is now used. Examples might include metrics that 
	emphasize the localization and discovery potential of a detector network.''} 

Some proposed ideas are currently under investigation, as reported in 
		Section \ref{sec:design-performance_metrics}. \\
	

\noindent\textbf{Building an international collaboration of 3G efforts}

Discussion during the Dawn III meeting resulted in one recommendation on the 
governance structure of a global 3G detector effort, which is quoted below along 
with a summary of progress. \\

\noindent \textit{``Global community building for internationally coordinated science case, and 
	detector design/development.''} 
The Dawn IV meeting and report are steps toward this global planning. 
See also Section \ref{sec:roadmap-dawn_meetings} for recommendations on the future 
		of Dawn meetings. 


