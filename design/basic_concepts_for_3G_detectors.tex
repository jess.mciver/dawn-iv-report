\subsection{Basic concepts for 3rd generation detectors}\label{sec:design-basic_concepts}

Third generation (3G) instruments, such as Einstein Telescope (ET) 
and Cosmic Explorer, will take advantage of the latest 
technological developments, as well as a new facility to extend 
detector baselines from 3-4~km to 10s of km. 

The ET concept relies on a triangular shaped facility, with 10~km 
long arms and three co-located instruments. Each instrument 
comprises of two detectors each, in a xylophone configuration in 
which one detector maximizes the sensitivity at low frequency, while 
the other detector maximizes the high frequency performance. The 
output of the two detectors are then combined to provide a 
broadband sensitivity. The main advantage of a triangular facility is 
the ability of resolving the polarization of gravitational waves without 
the need of additional facilities; the main drawback is the complexity 
of installing and commissioning multiple instruments in the same facility.

The main advantage of the xylophone configuration is to decouple 
technologies that are difficult to coexist in a single detector, like, for 
example, cryogenics ($\sim$20K) and high power operations. The ET 
xylophone concept calls for a low frequency cryogenic detector, with 
low circulating power, and a high frequency room temperature detector 
with high circulating power.

Even for the xylophone concept the main drawback is complexity, as the 
two types of detectors have very different characteristics and they require 
dedicated commissioning efforts (in other words, lessons learned from one 
detector are not necessarily relevant for the other). 

The Cosmic Explorer concept relies on an L-shaped configuration with 
40~km arms. Increasing the arm length beyond the existing 4~km facilities 
is crucial to take advantage of the scaling of fundamental noise with 
length~\cite{TheLIGOScientific:2017bvp}. Arms of 40~km length appear to 
be optimal; this length leads to a first null in sensitivity at 3.75 kHz, 
and an arm length longer than 40~km would reduce the useful detector bandwidth 
and negatively impact some 
science goals~\cite{Essick:2017kbt}.

The Cosmic Explorer approach provides the highest sensitivity in the 100 
Hz region. In the absence of ET, two Cosmic Explorer instruments would be 
needed to resolve the polarization of gravitational waves.

The potential of the 3G detector network and the criteria to optimize its 
science performance is an active area of research.
The 3G detectors aim to improve the sensitivity over the 2G network by more 
than a factor of 10, with their astrophysical reach targeting cosmological 
distances. Coupling this fact with the co-location of multiple detectors in the 
original ET design, it emerged from Dawn IV that the typical detector 
sensitivity curve is not anymore a representative figure of merit. 
%\checkme{(Option to include new sensitivity plot made by Evan after Dawn IV)} 
The astrophysical reach for binary systems can be more accurately represented 
by redshift vs. source mass, as illustrated in Figure \ref{fig:cosmo_reach_snr}, 
showing how 
ET and Cosmic Explorer cover a wide range of binary sources.

\begin{figure}\label{fig:cosmo_reach_snr} 
\includegraphics[width=0.5\textwidth]{figures/cumulative_snr_ttc.pdf}
\includegraphics[width=0.5\textwidth]{figures/gw_horizons.pdf}
\caption{
	Accumulated SNR for 2G and 3G 
	detectors vs time till merger (left). Cosmological reach of 
	2G and 3G detectors for CBC (right).  \textit{Credit: Evan Hall and John Miller (MIT)}~\cite{Hall:2019}.
	}
\end{figure}

The ET and Cosmic Explorer original designs where motivated by the historical 
context in which they evolved:

\begin{itemize}
	\item ET was designed to be a stand-alone 3G underground facility, 
	encompassing multiple detectors in a 10km triangular baseline to best 
	extract polarization information from the gravitational wave sources;
	\item Cosmic Explorer was designed to complement ET, by extending 
	the astrophysical reach for binary neutron star systems. To take advantage 
	of the scaling of noise sources with length, Cosmic Explorer was designed 
	as an L-shaped, 40km detector.
\end{itemize}

Both ET and Cosmic Explorer designs are currently evolving to take into account 
the needs of a global detector network. Moreover, exploration of the scientific 
advantages and positioning of a third 
3G gravitational-wave detector has begun.  This third detector could draw from 
both ET and CE designs to complement and complete a 3G network able of 
precision pointing.

As noted in Section \ref{sec:roadmap-path_forward}, 2.5G detector
designs that 
leverage new technologies to extend the reach of existing facilities 
are also under development and may serve as a bridge 
between 2G and 3G detectors. 
For example, LIGO 
Voyager would extend LIGO detector sensitivity 
beyond A+ 
by including lower loss 
mechanical coatings operating at room temperature for 1064nm 
light and broadband squeezing enhancement. The current 
LIGO Voyager concept ~\cite{TheLIGOScientific:2016aqa}
calls for a new wavelength of the laser light (1550~nm - 2~um) and 
new materials (silicon optics, amorphous Silicon coatings) which 
would require cryogenic operations at 123~K.




