\subsection{Performance metrics}\label{sec:design-performance_metrics}

The effort to bring gravitational-wave astronomy into a new era 
with a set of third-generation detectors is already well under way, 
and design studies are proceeding with two main thrusts. 
On one hand, the 
gravitational-wave community is identifying a list of science goals 
for the third-generation network, attempting to answer questions 
around neutron stars, the history of star formation, cosmological 
evolution, strong gravity, and other areas. On the other hand, the 
community is also considering what the third-generation 
ground-based gravitational-wave network will look like: how many 
facilities to build, where to locate and orient them, and what kind of 
detectors to put inside them.

The parameters characterizing the third-generation network should 
be chosen to best achieve the science goals. In order to provide a 
quantitative link between the network parameters and the science 
goals, a series of metrics (equivalently, figures of merit) should be 
established to evaluate the performance of a fiducial third-generation 
network. Figure-of-merit studies for gravitational-wave networks have 
already been undertaken~\cite{Raffai:2013fgy, Hu:2015qwe, 
Vitale:2017pnf, Vitale:2018lkx, Mills:2018dfh, Michimura:2018xby} 
in some cases using figures of merit 
to optimize detector placement/configuration and in other cases 
exploring the figure-of-merit performance of a few plausible network 
configurations.

There has been a preliminary exploration of the 
performance of a large number of potential three-detector networks, 
consisting of various combinations of Voyager, Einstein Telescope, 
and Cosmic Explorer, with random facility locations and orientations 
around the globe. A large, randomized ensemble enables us to 
exhaustively analyze the parameter space of possible three-detector 
networks. In this preliminary study, the figures of merit under 
configuration are sky localization, signal-to-noise ratio, and luminosity 
distance uncertainty for BNSs at redshift 0.3, along with the 
signal-to-noise ratio of an unmodeled (white in strain) high-frequency 
source. For each network realization, the figures of merit are computed 
using an ensemble of sources distributed isotropically in sky location 
and inclination. The sky localization and luminosity distance uncertainties 
are computed via Fisher matrix. For each figure of merit, both the 
median and the best 1\% of the events are computed.\\

Already several conclusions can be drawn from this preliminary study:

\begin{enumerate}
	\item The single largest predictor of network performance is the 
	composition of the network: even when the facility locations and 
	orientations are randomized, different classes of networks 
	naturally cluster together when plotted according to their figure of 
	merit performance.
	\item Generally, networks of three third-generation detectors 
	outperform networks of one third-generation
	detector supplemented with two second-generation detectors, 
	particularly for localization and distance uncertainty.
	\item Networks which achieve good figure-of-merit performance 
	when optimized according to the median event also achieve good 
	performance when optimized for the best 1\% of events, and vice 
	versa.
\end{enumerate}

This study has several natural extensions, such as including other 
figures of merit such as mass uncertainty and polarization selectivity, 
and a wider variety of networks, including networks with more or fewer 
than three detectors~\cite{Hall:2019}.





